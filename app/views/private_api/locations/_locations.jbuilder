json.locations do
  json.array! locations do |location|
    json.extract!(location, :name, :secret_code)
  end
end
