class PanelProvider < ActiveRecord::Base

  CODE_TO_PRICE_STRATEGY = {
    'PREMIUM' => :PremiumPriceStrategy,
    'BASIC'   => :BasicPriceStrategy,
    'VIP'     => :VipPriceStrategy
  }

  def price
    PriceStrategy.const_get(CODE_TO_PRICE_STRATEGY.fetch(code)).new.call
  end

  # I assume that pricing logic is a serious thing, stable enough to hardcode it.
  # TODO: Extract to separate files
  class PriceStrategy
    class BasicPriceStrategy
      def call
        PriceCalculator::HtmlNodePriceCalculator.new(Source.time_com.content).call
      end
    end

    class PremiumPriceStrategy
      def call
        PriceCalculator::LetterAPriceCalculator.new(Source.time_com.content).call
      end
    end

    class VipPriceStrategy
      def call
        PriceCalculator::BTagsPriceCalculator.new(Source.google_news.content).call
      end
    end
  end

end
