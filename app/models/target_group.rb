class TargetGroup < ActiveRecord::Base
  acts_as_nested_set

  belongs_to :panel_provider
end
