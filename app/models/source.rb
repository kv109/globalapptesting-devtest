class Source < ActiveRecord::Base

  # I assume that sources are stable enough to hardcode it
  scope :time_com,    -> { where(code: :time_com).first }
  scope :google_news, -> { where(code: :google_news).first }

  validates :code, presence: true, uniqueness: true
  validates :url,  presence: true, format: URI::regexp, uniqueness: true

  def update_content_and_save!
    update_content! && save!
  end

  def update_content!
    # TODO: save only if response is 200
    # TODO: raise if could not update for a long time (store this time in db?)
    self.content = SingleSourceCrawler.new(url).body
  end

  class << self
    def update_sources!
      # TODO: Each source should be updated in a separate Job
      all.each do |source|
        puts "Updating content for [#{source.url}]..."
        source.update_content_and_save!
        puts '...updated!'
      end
    end
  end

end
