class PublicApi::LocationsController < PublicApiController
  def country_code
    @data = Call::Locations::CountryCodeCall.new(params).call
  end
end
