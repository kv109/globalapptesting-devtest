class PrivateApi::LocationsController < PrivateApiController
  def country_code
    @data = Call::Locations::CountryCodeCall.new(params).call
  end
end
