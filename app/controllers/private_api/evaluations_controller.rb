class PrivateApi::EvaluationsController < ApplicationController
  def evaluate_target
    @data = Call::Evaluations::EvaluateTargetCall.new(params).call
  end
end
