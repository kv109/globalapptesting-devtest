class ApplicationController < ActionController::API
  rescue_from CallParams::InvalidParamsError, with: :errors_to_json

  private

  def errors_to_json(e)
    render json: e.errors, status: 422
  end
end
