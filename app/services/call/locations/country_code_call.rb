class Call::Locations::CountryCodeCall < Call

  attr_reader :params

  def initialize(params)
    @params = clean_params(params)
  end

  def call
    { locations: locations }
  end

  private

  def locations
    Query::Locations::ByCountriesPanelProviders.new(countries).call
  end

  def countries
    Query::Countries.new.by_country_codes(params['country_code'])
  end

  def params_class
    Class.new(CallParams) do

      keys :country_code

      validates :country_code, presence: true

      def normalize
        params['country_code'].upcase!
      end
      
    end
  end

end
