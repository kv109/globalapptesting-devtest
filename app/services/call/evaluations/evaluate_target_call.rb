class Call::Evaluations::EvaluateTargetCall < Call

  attr_reader :params

  def initialize(params)
    @params = clean_params(params)
  end

  def call
    { price: price }
  end

  private

  # I couldn't figure out how exactly params other than :country_code affects price
  def price
    # TODO: Crashes if PanelProvider couldn't be found
    # TODO: Some consistent way of handling RecordNotFound should be introduced
    panel_provider.price
  end

  def panel_provider
    Query::PanelProviders::ByCountries.new(countries).call.first
  end

  def countries
    Query::Countries.new.by_country_codes(params['country_code'])
  end

  def params_class
    Class.new(CallParams) do

      keys :country_code, :target_group_id, :locations

      validates :country_code, :target_group_id, :locations, presence: true
      validate :validate_locations

      def validate_locations
        if locations.is_a?(Array)
          if !locations.all?(&method(:valid_location_hash?))
            errors.add(:locations, 'each location has to be Hash with keys: "id" and "panel_size"')
          end
        else
          errors.add(:locations, 'locations have to be Array')
        end
      end

      def valid_location_hash?(hash)
        hash.has_key?('id') && hash.has_key?('panel_size')
      end
    end

  end

end
