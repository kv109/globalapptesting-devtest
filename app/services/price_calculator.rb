class PriceCalculator
  def call
    raise NotImplementedError, 'has to return Numeric'
  end
end
