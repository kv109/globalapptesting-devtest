# Responsibility: Calculate price based on "b" tags occurrences (<b>) in pure, not parsed JSON content
class PriceCalculator::BTagsPriceCalculator

  attr_reader :json

  def initialize(json)
    @json = json
  end

  def call
    b_tags_count
  end

  private

  def b_tags_count
    json.scan('\u003cb\u003e').count
  end

end
