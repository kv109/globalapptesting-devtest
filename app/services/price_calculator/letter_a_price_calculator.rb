# Responsibility: Calculate price based on visible "a" letter occurrences in HTML content
class PriceCalculator::LetterAPriceCalculator < PriceCalculator

  A_LETTER = 'a'
  FACTOR = 0.01

  attr_reader :html

  def initialize(html)
    @html = html
  end

  def call
    page_visible_content.count(A_LETTER) * FACTOR
  end

  private

  # It's a very primitive "algorithm"
  def page_visible_content
    n_html = Nokogiri::HTML(html)
    n_html.css('body').tap do |doc|
      doc.search('script').unlink
    end.text.gsub(/\W/, '')
  end

end
