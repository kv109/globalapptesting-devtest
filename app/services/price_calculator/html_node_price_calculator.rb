# Responsibility: Calculate price based on HTML nodes number in HTML content
class PriceCalculator::HtmlNodePriceCalculator < PriceCalculator

  FACTOR = 0.01

  attr_reader :html

  def initialize(html)
    @html = html
  end

  def call
    html_nodes_count * FACTOR
  end

  private

  # It's a very primitive "algorithm"
  def html_nodes_count
    n_html = Nokogiri::XML(html)
    count = 0
    n_html.traverse { |node| count += 1 }
    count
  end

end
