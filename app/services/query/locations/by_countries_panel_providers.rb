class Query::Locations::ByCountriesPanelProviders < Query::Locations

  attr_reader :countries

  def initialize(countries)
    @countries = countries
  end

  def call
    Query::Locations::ByLocationGroups.new(location_groups).call
  end

  private

  def location_groups
    Query::LocationGroups::ByCountriesPanelProviders.new(countries).call
  end

end
