class Query::Locations::ByLocationGroups < Query::Locations

  attr_reader :location_groups

  def initialize(location_groups)
    @location_groups = location_groups
  end

  def call
    scope.where(id: LocationGroupsLocation.where(location_group: location_groups).select(:location_id))
  end

end
