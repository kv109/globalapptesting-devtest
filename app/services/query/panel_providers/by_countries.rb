class Query::PanelProviders::ByCountries < Query::PanelProviders

  attr_reader :countries

  def initialize(countries)
    @countries = countries
  end

  def call
    scope.where(id: countries.select(:panel_provider_id))
  end

end
