class Query::Countries

  def by_country_codes(*country_codes)
    scope.where(country_code: country_codes)
  end

  def scope
    Country
  end

end
