class Query::TargetGroups::ByCountriesPanelProviders < Query::TargetGroups

  attr_reader :countries

  def initialize(countries)
    @countries = countries
  end

  def call
    scope.where(panel_provider: panel_providers)
  end

  private

  def panel_providers
    Query::PanelProviders::ByCountries.new(countries).call
  end

end
