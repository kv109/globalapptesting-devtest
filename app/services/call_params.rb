# Responsibility: Params validation and normalization.
class CallParams

  include ActiveModel::Validations

  attr_reader :params

  def initialize(params)
    @params = params.to_hash
  end

  def clean_params
    raise InvalidParamsError.new(errors) unless valid?
    normalize!
  end

  class << self
    def keys(*args)
      delegate *args, to: :params_struct, allow_nil: true
    end

    # Because of "You need to supply a name argument when anonymous class given" error.
    # ActiveModel::Validations require #model_name
    def model_name
      ActiveModel::Name.new(self, nil, 'ParamsValidator')
    end
  end

  private

  def normalize!
    params.tap do
      normalize
    end
  end

  def normalize
    # TODO: Create DSL like
    # normalize do |params|
    #   params['code'].upcase!
    # end
  end

  def params_struct
    @params_struct ||= OpenStruct.new(@params)
  end

  class InvalidParamsError < StandardError
    attr_reader :errors

    def initialize(errors)
      @errors = errors
    end
  end

end
