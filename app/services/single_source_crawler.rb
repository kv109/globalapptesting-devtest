# Extra layer on HTTParty (for the future)
class SingleSourceCrawler < Struct.new(:url)

  delegate :body, to: :response

  private

  def response
    HTTParty.get(url)
  end

end
