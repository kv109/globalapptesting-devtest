class Call

  def call
    raise NotImplementedError, 'has to return Hash'
  end

  private

  def clean_params(params)
    params_class.new(params).clean_params
  end

  def params_class
    CallParams
  end

end
