# Responsibility: Analyze content in terms of given patter
class Scraper::PatternScraper < Struct.new(:content, :pattern)

  def count
    content.scan(pattern).count
  end

end