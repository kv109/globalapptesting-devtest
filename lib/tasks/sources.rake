namespace :sources do

  desc 'updates all sources content'
  task update: :environment do
    Source.update_sources!
  end
end

