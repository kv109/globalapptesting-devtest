require 'rails_helper'

RSpec.describe Source, type: :model do
  let(:url) { 'http://time.com' }
  let!(:record) { described_class.create!(url: url, code: :time_com) }

  before do
    stub_request(:get, url).to_return(body: '<b>TIME</b>')
  end

  describe '#download!' do
    subject { -> { record.update_content_and_save! } }
    it { is_expected.to change { record.reload.content }.from(nil).to('<b>TIME</b>') }
  end

  describe '#download' do
    subject { -> { record.update_content! } }
    it { is_expected.to change     { record.content }.from(nil).to('<b>TIME</b>') }
    it { is_expected.to_not change { record.reload.content } }
  end
end
