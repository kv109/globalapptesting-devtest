require 'rails_helper'

RSpec.describe PublicApi::LocationsController, type: :controller do

  describe 'GET #by_country_code' do
    it 'returns http success' do
      get :country_code, country_code: 'POL'
      expect(response).to have_http_status(:success)
    end
  end

end
