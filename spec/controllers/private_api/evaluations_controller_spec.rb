require 'rails_helper'

RSpec.describe PrivateApi::EvaluationsController, type: :controller do

  describe 'GET #evaluate_target' do
    it 'returns http success' do
      get :evaluate_target
      expect(response).to have_http_status(:success)
    end
  end

end
