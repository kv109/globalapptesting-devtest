=begin
If you wonder why I use this strange convention with so many contexts, it's because it can give me following, very readable output:
$ rspec spec/services/price_calculator/letter_a_price_calculator_spec.rb --format=doc

PriceCalculator::LetterAPriceCalculator
  #call
    when source has one "a"
      should eql 0.01
    when source has two hundreds "a"
      should eql 2.0
    when source has no content
      should be zero
    when source has tags with "a" but no "a"
      should be zero
    when source has tags with "a" and 100 "a"
      should eql 1.0
=end
require 'rails_helper'

describe PriceCalculator::LetterAPriceCalculator do
  let(:instance) { described_class.new(html) }

  describe '#call' do
    subject { instance.call }

    context 'when source has one "a"' do
      let(:html) { 'Some text with "a"' }
      it { is_expected.to eql 0.01 }
    end

    context 'when source has two hundreds "a"' do
      let(:html) { 'Some text with "a"' * 200 }
      it { is_expected.to eql 2.0 }
    end

    context 'when source has no content' do
      let(:html) { nil }
      it { is_expected.to be_zero }
    end

    context 'when source has tags with "a" but no "a"' do
      let(:html) { '<a>link</a>' }
      it { is_expected.to be_zero }
    end

    context 'when source has tags with "a" and 100 "a"' do
      let(:html) { '<a>a</a>' * 100 }
      it { is_expected.to eql 1.0 }
    end

  end
end