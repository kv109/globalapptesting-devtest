require 'rails_helper'

describe Scraper::PatternScraper do
  let(:instance) { described_class.new(content, pattern) }

  describe '#count' do
    subject { instance.count }

    let(:pattern) { 'Find me' }

    context 'when content is empty' do
      let(:content) { '' }
      it { is_expected.to eql 0 }
    end

    context 'when pattern does not occur' do
      let(:content) { 'This content doesnt have what youre looking for' }
      it { is_expected.to eql 0 }
    end

    context 'when pattern occurs three times' do
      let(:content) { 'Some text then Find me some other text then Find me and Find me' }
      it { is_expected.to eql 3 }
    end

    context 'when pattern is a String' do
      let(:content) { 'FIND ME' }
      it 'is NOT case sensitive' do
        is_expected.to eql 0
      end
    end

    context 'when pattern is a Regexp' do
      let(:pattern) { /Find me/ }
      let(:content) { 'FIND ME' }

      it 'is NOT case sensitive' do
        is_expected.to eql 0
      end

      context 'with insensitive flag' do
        let(:pattern) { /Find me/i }

        it 'is case sensitive' do
          is_expected.to eql 1
        end
      end
    end

  end
end