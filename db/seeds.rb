# Note: It generates some random data with Faker. Normally I would never allow that, but this time I wanted to have some fun.
class Seeds
  class << self
    def uuid
      SecureRandom.uuid
    end

    def code
      SecureRandom.hex(6)
    end

    def create_target_group_child(parent)
      TargetGroup.create(parent: parent,
                         panel_provider: parent.panel_provider,
                         name: "Level: #{parent.depth + 1}-#{parent.id}-#{parent.reload.children.count}",
                         external_id: Seeds.uuid,
                         secret_code: Seeds.code
      )
    end
  end
end

Source.create(code: :time_com, url: 'http://time.com').tap do |source|
  # TODO: Load content from file
  source.update_content_and_save!
end

Source.create(code: :google_news, url: 'https://ajax.googleapis.com/ajax/services/feed/find?v=1.0&q=news').tap do |source|
  # TODO: Load content from file
  source.update_content_and_save!
end

PanelProvider.create(code: 'PREMIUM').tap do |panel_provider|
  Country.create(country_code: 'POL', panel_provider: panel_provider).tap do |country|
    LocationGroup.create(name: 'Biedronka & Stonka', country: country, panel_provider: panel_provider)
  end

  TargetGroup.create(
    panel_provider: panel_provider,
    name:           '1st Root TG',
    external_id:    Seeds.uuid,
    secret_code:    Seeds.code
  ).tap do |target_group|
    5.times do
      Seeds.create_target_group_child(target_group).tap do |target_group|
        4.times do
          Seeds.create_target_group_child(target_group).tap do |target_group|
            7.times do
              Seeds.create_target_group_child(target_group).tap do |target_group|
                2.times do
                  Seeds.create_target_group_child(target_group)
                end
              end
            end
          end
        end
      end
    end
  end

  TargetGroup.create(
    panel_provider: panel_provider,
    name:           '2nd Root TG',
    external_id:    Seeds.uuid,
    secret_code:    Seeds.code
  ).tap do |target_group|
    4.times do
      Seeds.create_target_group_child(target_group).tap do |target_group|
        3.times do
          Seeds.create_target_group_child(target_group).tap do |target_group|
            2.times do
              Seeds.create_target_group_child(target_group).tap do |target_group|
                1.times do
                  Seeds.create_target_group_child(target_group)
                end
              end
            end
          end
        end
      end
    end
  end
end

PanelProvider.create(code: 'BASIC').tap do |panel_provider|
  Country.create(country_code: 'USA', panel_provider: panel_provider).tap do |country|
    LocationGroup.create(name: 'Unhealthy Foods LLC', country: country, panel_provider: panel_provider)
  end

  TargetGroup.create(
      panel_provider: panel_provider,
      name:           '3rd Root TG',
      external_id:    Seeds.uuid,
      secret_code:    Seeds.code
  ).tap do |target_group|
    1.times do
      Seeds.create_target_group_child(target_group).tap do |target_group|
        1.times do
          Seeds.create_target_group_child(target_group).tap do |target_group|
            1.times do
              Seeds.create_target_group_child(target_group).tap do |target_group|
                1.times do
                  Seeds.create_target_group_child(target_group).tap do |target_group|
                    1.times do
                      Seeds.create_target_group_child(target_group)
                    end
                  end
                end
              end
            end
          end
        end
      end
    end
  end

end

PanelProvider.create(code: 'VIP').tap do |panel_provider|
  Country.create(country_code: 'ESP', panel_provider: panel_provider).tap do |country|
    LocationGroup.create(name: 'El Sol & Sol', country: country, panel_provider: panel_provider)
    LocationGroup.create(name: 'Madrido Papaya', country: country, panel_provider: panel_provider)
  end

  TargetGroup.create(
      panel_provider: panel_provider,
      name:           '4th Root TG',
      external_id:    Seeds.uuid,
      secret_code:    Seeds.code
  ).tap do |target_group|
    8.times do
      Seeds.create_target_group_child(target_group).tap do |target_group|
        8.times do
          Seeds.create_target_group_child(target_group).tap do |target_group|
            8.times do
              Seeds.create_target_group_child(target_group)
            end
          end
        end
      end
    end
  end

end

20.times do |i|
  Location.create(
      name:            "#{Faker::Book.title} #{%w(Pub Restaurant Cafe Bakery).sample} #{i}",
      external_id:     Seeds.uuid,
      secret_code:     Seeds.code,
      location_groups: LocationGroup.all.sample(2)
  )
end