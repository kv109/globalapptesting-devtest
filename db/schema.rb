# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160323092548) do

  create_table "countries", force: :cascade do |t|
    t.string   "country_code",      null: false
    t.integer  "panel_provider_id", null: false
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
  end

  add_index "countries", ["panel_provider_id"], name: "index_countries_on_panel_provider_id"

  create_table "location_groups", force: :cascade do |t|
    t.string   "name",              null: false
    t.integer  "country_id",        null: false
    t.integer  "panel_provider_id", null: false
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
  end

  add_index "location_groups", ["country_id"], name: "index_location_groups_on_country_id"
  add_index "location_groups", ["panel_provider_id"], name: "index_location_groups_on_panel_provider_id"

  create_table "location_groups_locations", id: false, force: :cascade do |t|
    t.integer "location_id",       null: false
    t.integer "location_group_id", null: false
  end

  add_index "location_groups_locations", ["location_group_id", "location_id"], name: "index_loc_groups_locs_on_loc_group_id_and_loc_id", unique: true

  create_table "locations", force: :cascade do |t|
    t.string   "name",        null: false
    t.string   "external_id", null: false
    t.string   "secret_code", null: false
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "locations", ["external_id"], name: "index_locations_on_external_id", unique: true
  add_index "locations", ["name"], name: "index_locations_on_name", unique: true
  add_index "locations", ["secret_code"], name: "index_locations_on_secret_code", unique: true

  create_table "panel_providers", force: :cascade do |t|
    t.string   "code",       null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "sources", force: :cascade do |t|
    t.string   "code",       null: false
    t.string   "url",        null: false
    t.text     "content"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "sources", ["code"], name: "index_sources_on_code", unique: true
  add_index "sources", ["url"], name: "index_sources_on_url", unique: true

  create_table "target_groups", force: :cascade do |t|
    t.string   "name"
    t.integer  "panel_provider_id",             null: false
    t.string   "external_id",                   null: false
    t.string   "secret_code",                   null: false
    t.integer  "parent_id"
    t.integer  "lft",                           null: false
    t.integer  "rgt",                           null: false
    t.integer  "depth",             default: 0, null: false
    t.integer  "children_count",    default: 0, null: false
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
  end

  add_index "target_groups", ["external_id"], name: "index_target_groups_on_external_id"
  add_index "target_groups", ["lft"], name: "index_target_groups_on_lft"
  add_index "target_groups", ["panel_provider_id"], name: "index_target_groups_on_panel_provider_id"
  add_index "target_groups", ["parent_id"], name: "index_target_groups_on_parent_id"
  add_index "target_groups", ["rgt"], name: "index_target_groups_on_rgt"
  add_index "target_groups", ["secret_code"], name: "index_target_groups_on_secret_code"

end
