class CreateTargetGroups < ActiveRecord::Migration
  def change
    create_table :target_groups do |t|
      t.string :name,               null: false
      t.belongs_to :panel_provider, null: false
      t.string :external_id,        null: false
      t.string :secret_code,        null: false
      t.integer :parent_id
      t.integer :lft,               null: false
      t.integer :rgt,               null: false
      t.integer :depth,             null: false, default: 0
      t.integer :children_count,    null: false, default: 0

      t.index :name
      t.index :panel_provider_id
      t.index :external_id
      t.index :secret_code
      t.index :parent_id
      t.index :lft
      t.index :rgt

      t.timestamps null: false
    end
    add_foreign_key :target_groups, :panel_providers
  end
end
