class CreateJoinTableLocationLocationGroup < ActiveRecord::Migration
  def change
    create_join_table :locations, :location_groups do |t|
      t.index [:location_group_id, :location_id], unique: true, name: 'index_loc_groups_locs_on_loc_group_id_and_loc_id'
    end
  end
end
