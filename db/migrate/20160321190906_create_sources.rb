class CreateSources < ActiveRecord::Migration
  def change
    create_table :sources do |t|
      t.string :code, null: false # URL can change so I need human readable UID
      t.string :url,  null: false
      t.text :content
      t.timestamps null: false
    end

    add_index :sources, :code, unique: true
    add_index :sources, :url, unique: true
  end
end
