class CreateCountries < ActiveRecord::Migration
  def change
    create_table :countries do |t|
      t.string :country_code, null: false
      t.belongs_to :panel_provider, index: true, null: false

      t.timestamps null: false
    end
    add_foreign_key :countries, :panel_providers
  end
end
