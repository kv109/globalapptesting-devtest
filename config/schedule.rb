# It requires loading the whole Rails env every minute, which is a bad idea.
# In real world I would use some more custom solution.
every 1.minute do
  rake 'sources:update'
end
