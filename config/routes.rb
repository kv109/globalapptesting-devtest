Rails.application.routes.draw do

  namespace :public_api, path: 'api' do
    get 'locations/:country_code' => 'locations#country_code'
  end

  namespace :private_api, path: 'api/private' do
    get 'locations/:country_code' => 'locations#country_code'
    post 'evaluate_target' => 'evaluations#evaluate_target' #TODO: GET => POST
  end

end
